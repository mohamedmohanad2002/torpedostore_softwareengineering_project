<?php

class connectDatabase{
    private $dbServername = "localhost";
    private $dbUsername = "root";
    private $dbPassword = "";
    private $dbName = "torpedostore";
    private $query;
    private $numOfRow;
    private $result;

    public function set_database_server_name($dbServername)
    {
        $this->dbServername = $dbServername;
    }

    public function set_database_username($dbUsername)
    {
        $this->dbUsername = $dbUsername;
    }

    public function set_database_password($dbPassword)
    {
        $this->dbPassword = $dbPassword;
    }

    public function set_database_name($dbName)
    {
        $this->dbName = $dbName;
    }

    public function connect_get_data($query)
    {
        $this->query = $query;

        $conn = mysqli_connect($this->dbServername, $this->dbUsername, $this->dbPassword, $this->dbName);
        if($conn->connect_error){
            die("Connection To Data Base Faild.. :( " . $conn->connect_error);
        }
        
        $this->result = mysqli_query($conn, $this->query);
        $this->numOfRow = mysqli_num_rows($this->result);
    }
	
	public function connect_insert_data($query)
    {
		$this->query = $query;
		
        try {
		  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		  
		  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		  $sql = $query;
		  
		  $conn->exec($sql);
		  echo "New record created successfully";
		} catch(PDOException $e) {
		  echo $sql . "<br>" . $e->getMessage();
		}

		$conn = null;
    }

    public function get_num_of_rows()
    {
        return $this->numOfRow;
    }

    public function get_result()
    {
        return $this->result;
    }

}