-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2021 at 06:34 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `torpedostore`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `accountID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Date` date NOT NULL,
  `Country` varchar(100) NOT NULL,
  `isSeller` tinyint(1) NOT NULL DEFAULT 0,
  `userImage` longblob DEFAULT '?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is user`s accounts table';

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`accountID`, `Name`, `Email`, `Password`, `Date`, `Country`, `isSeller`, `userImage`) VALUES
(1, 'Mohamed Mohanad', 'mohamedmohanad0852@gmail.com', 'honda434757', '2002-01-19', 'Egypt', 0, ''),
(2, 'Mohamed Wael Youns', 'BlodyWolf144@gmail.com', 'blody123456789', '2000-05-08', 'Egypt', 0, 0x3f),
(3, 'Mohamed Wael Sherif', 'Tapuegold33@gmail.com', 'moo1823', '1999-02-03', 'Egypt', 0, 0x3f),
(4, 'Mohamed Fahmy', 'fahmy1777@gmail.com', 'gamer753214', '2000-06-04', 'Egypt', 0, 0x3f),
(5, 'Mohamed Hany', 'lionheart@gmail.com', 'saytra85263', '2002-01-29', 'Egypt', 0, 0x3f);

-- --------------------------------------------------------

--
-- Table structure for table `account_addresses`
--

CREATE TABLE `account_addresses` (
  `accountID` int(11) NOT NULL,
  `Address` varchar(300) NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account_addresses`
--

INSERT INTO `account_addresses` (`accountID`, `Address`, `ID`) VALUES
(1, 'حارة ابو الحمد-ترعة زنين-المعتمدية-بولاق الدكرور-الجيزة', 1),
(2, 'السيدة زينب-شارع المصريين', 2),
(3, 'مرسي مروح-العالمين', 3),
(4, 'حلوان-شارع الديوك', 4),
(5, 'بولاق الدكرور-ارض اللواء', 5);

-- --------------------------------------------------------

--
-- Table structure for table `account_phones`
--

CREATE TABLE `account_phones` (
  `accountID` int(11) NOT NULL,
  `phoneNumber` varchar(12) NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is user`s phones table';

--
-- Dumping data for table `account_phones`
--

INSERT INTO `account_phones` (`accountID`, `phoneNumber`, `ID`) VALUES
(1, '01143475759', 1),
(2, '01007603102', 2),
(3, '01015185691', 3),
(4, '01554351103', 4),
(5, '01144128849', 5);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categorieID` int(11) NOT NULL,
  `categorieName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is categories table';

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categorieID`, `categorieName`) VALUES
(1, 'Gaming'),
(2, 'Mobile Phones, Tablets & Accessories'),
(3, 'Computers, IT & Networking'),
(4, 'Vehicle Parts & Accessories'),
(5, 'Office Products & Supplies'),
(6, 'Toys');

-- --------------------------------------------------------

--
-- Table structure for table `compare_products`
--

CREATE TABLE `compare_products` (
  `productID` int(11) NOT NULL,
  `productImage` longblob NOT NULL,
  `productDescription` text NOT NULL,
  `productRate` int(11) NOT NULL,
  `productPrice` double NOT NULL,
  `sellerBy` varchar(100) NOT NULL,
  `productQuantity` int(11) NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `favourite_products`
--

CREATE TABLE `favourite_products` (
  `productID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `Date` date NOT NULL DEFAULT current_timestamp(),
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is favourit products table';

-- --------------------------------------------------------

--
-- Table structure for table `feedback_system`
--

CREATE TABLE `feedback_system` (
  `feedbackID` int(11) NOT NULL,
  `feedbackContent` text NOT NULL,
  `userID` int(11) NOT NULL,
  `feedbvackDate` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is feedback about system table';

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productID` int(11) NOT NULL,
  `categorieID` int(11) NOT NULL,
  `subcategorieID` int(11) NOT NULL,
  `productName` text NOT NULL,
  `sellerID` int(11) NOT NULL,
  `productPrice` double NOT NULL,
  `productAddedDate` date NOT NULL DEFAULT current_timestamp(),
  `productManufacturer` varchar(200) NOT NULL DEFAULT 'torpedostore',
  `productWarranty` int(11) NOT NULL DEFAULT 0,
  `productDescription` text NOT NULL,
  `productQuantity` int(11) NOT NULL,
  `Discount` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is product`s details table';

-- --------------------------------------------------------

--
-- Table structure for table `product_feedbacks`
--

CREATE TABLE `product_feedbacks` (
  `productID` int(11) NOT NULL,
  `feedbackContent` text NOT NULL,
  `userID` int(11) NOT NULL,
  `Date` date NOT NULL DEFAULT current_timestamp(),
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is product`s feedbacks details table';

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `productID` int(11) NOT NULL,
  `image` longblob NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_rate`
--

CREATE TABLE `product_rate` (
  `productID` int(11) NOT NULL,
  `rateValue` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `Date` date NOT NULL DEFAULT current_timestamp(),
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is product`s rates table';

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `subcategorieID` int(11) NOT NULL,
  `categorieID` int(11) NOT NULL,
  `subcategorieName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is subcategories table';

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`subcategorieID`, `categorieID`, `subcategorieName`) VALUES
(1, 1, 'Game Console'),
(2, 1, 'Game Gadgets & Accessories'),
(3, 1, 'Video Games'),
(4, 1, 'VR Gadgets'),
(5, 2, 'Mobile Phones'),
(6, 2, 'Screen Protectors'),
(7, 2, 'Tablets'),
(8, 2, 'Smart Watches'),
(9, 3, 'Computer & Laptop Accessories'),
(10, 3, 'Laptops & Netbooks'),
(11, 3, 'Software'),
(12, 3, 'Networking & Accessories'),
(13, 4, 'Car Audio'),
(14, 4, 'Car Parts'),
(15, 4, 'Tiers & Wheels'),
(16, 5, 'Fax Machine'),
(17, 5, 'Office Equipment'),
(18, 5, 'Printers'),
(19, 5, 'Office Supplies'),
(20, 6, 'Bikes, Scooters'),
(21, 6, 'Toys'),
(22, 6, 'Costumes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`accountID`);

--
-- Indexes for table `account_addresses`
--
ALTER TABLE `account_addresses`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Account_ID_ADDRESS` (`accountID`);

--
-- Indexes for table `account_phones`
--
ALTER TABLE `account_phones`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Account_ID` (`accountID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categorieID`);

--
-- Indexes for table `compare_products`
--
ALTER TABLE `compare_products`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID_Compare` (`productID`);

--
-- Indexes for table `favourite_products`
--
ALTER TABLE `favourite_products`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID_Favourite` (`productID`),
  ADD KEY `User_ID_Favourite` (`userID`);

--
-- Indexes for table `feedback_system`
--
ALTER TABLE `feedback_system`
  ADD PRIMARY KEY (`feedbackID`),
  ADD KEY `Account_ID_Feedback_System` (`userID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productID`),
  ADD KEY `Categorie_ID_Product` (`categorieID`),
  ADD KEY `Subcategorie_ID_Product` (`subcategorieID`),
  ADD KEY `Seller_ID_Product` (`sellerID`);

--
-- Indexes for table `product_feedbacks`
--
ALTER TABLE `product_feedbacks`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID_Feedback` (`productID`),
  ADD KEY `User_ID_Feedback` (`userID`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID_Images` (`productID`);

--
-- Indexes for table `product_rate`
--
ALTER TABLE `product_rate`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID_Rate` (`productID`),
  ADD KEY `User_ID_Rate` (`userID`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`subcategorieID`),
  ADD KEY `Categorie_ID_SubCategorie` (`categorieID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `accountID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `account_addresses`
--
ALTER TABLE `account_addresses`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `account_phones`
--
ALTER TABLE `account_phones`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categorieID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `compare_products`
--
ALTER TABLE `compare_products`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourite_products`
--
ALTER TABLE `favourite_products`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feedback_system`
--
ALTER TABLE `feedback_system`
  MODIFY `feedbackID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_feedbacks`
--
ALTER TABLE `product_feedbacks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_rate`
--
ALTER TABLE `product_rate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `subcategorieID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_addresses`
--
ALTER TABLE `account_addresses`
  ADD CONSTRAINT `Account_ID_ADDRESS` FOREIGN KEY (`accountID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `account_phones`
--
ALTER TABLE `account_phones`
  ADD CONSTRAINT `Account_ID` FOREIGN KEY (`accountID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `compare_products`
--
ALTER TABLE `compare_products`
  ADD CONSTRAINT `Product_ID_Compare` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favourite_products`
--
ALTER TABLE `favourite_products`
  ADD CONSTRAINT `Product_ID_Favourite` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `User_ID_Favourite` FOREIGN KEY (`userID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedback_system`
--
ALTER TABLE `feedback_system`
  ADD CONSTRAINT `Account_ID_Feedback_System` FOREIGN KEY (`userID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `Categorie_ID_Product` FOREIGN KEY (`categorieID`) REFERENCES `categories` (`categorieID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Seller_ID_Product` FOREIGN KEY (`sellerID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Subcategorie_ID_Product` FOREIGN KEY (`subcategorieID`) REFERENCES `sub_categories` (`subcategorieID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_feedbacks`
--
ALTER TABLE `product_feedbacks`
  ADD CONSTRAINT `Product_ID_Feedback` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `User_ID_Feedback` FOREIGN KEY (`userID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `Product_ID_Images` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_rate`
--
ALTER TABLE `product_rate`
  ADD CONSTRAINT `Product_ID_Rate` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `User_ID_Rate` FOREIGN KEY (`userID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `Categorie_ID_SubCategorie` FOREIGN KEY (`categorieID`) REFERENCES `categories` (`categorieID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
